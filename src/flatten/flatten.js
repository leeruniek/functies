import { unnest, flatten as _flatten } from "ramda"

import { deprecate } from "../deprecate/deprecate"
import { map } from "../map/map"
import { reduce } from "../reduce/reduce"
import { push } from "../push/push"

export const flattenOne = deprecate(unnest, "flattenOne", "unnest")

export const flatten = deprecate(_flatten, "flatten")

/**
 * An alternate version of flatten that treats nested arrays as branches,
 * constructing a set of arrays which represent the permutations of
 * concatenations possible when taking a single value from each branch.
 *
 * Unlike flatten, flattenBranches does not recurse on subelements.
 *
 * WARNING:
 * As a permutation function, it's subject to combinatorial explosion.
 *
 * @example
 * flattenBranches([1, 2, [3, 4]]) =
 *   [[1, 2, 3], [1, 2, 4]]
 *
 * flattenBranches([1, [2, 3], [4, 5]]) =
 *   [[1, 2, 4], [1, 2, 5], [1, 3, 4], [1, 3, 5]]
 */
export const flattenBranches = reduce((acc, val) => {
  if (Array.isArray(val)) {
    const addItems = arr => map(item => push(item)(arr))(val)

    const nextBranches = map(addItems)(acc)

    return unnest(nextBranches)
  }

  if (acc.length === 0) {
    return [[val]]
  }

  return map(push(val))(acc)
}, [])
