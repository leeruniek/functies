import { type as _type } from "ramda"

import { deprecate } from "../deprecate/deprecate"

export const type = deprecate(_type, "type")
