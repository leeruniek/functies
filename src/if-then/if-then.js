import { identity, ifElse, curryN } from "ramda"

import { deprecate } from "../deprecate/deprecate"

// Needed because Ramda's ifElse requires providing onFalse
const ifThenWrapper = (...args) => {
  if (args.length < 2) {
    console.log(
      "functies: ifThen called without onTrue function, this will return the argument passed to the return value"
    )
  }

  const argsWithDefaults = Object.assign([() => true, identity, identity], args)

  return ifElse(...argsWithDefaults)
}

export const ifThen = deprecate(
  ifThenWrapper,
  "ifThen",
  "ifElse",
  "Always provide the onFalse function to ifElse"
)
