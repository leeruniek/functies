import test from "tape"
import { ifThen } from ".."

test("core::ifThen", t => {
  t.equal(
    ifThen(() => true, () => 1, () => 2)("A"),
    1,
    "returns the result of thenFn if conditionFn returns true"
  )

  t.equal(
    ifThen(() => false, () => 1, () => 2)("A"),
    2,
    "returns the result of elseFn if conditionFn returns false"
  )

  t.equal(
    ifThen(() => true, () => 1)("A"),
    1,
    "returns the result of thenFn if conditionFn returns true and no elseFn is provided"
  )

  t.equal(
    ifThen(() => false, () => 1)("A"),
    "A",
    "returns the input value by default"
  )

  t.end()
})
