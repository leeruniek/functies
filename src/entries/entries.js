import { fromPairs } from "ramda"

import { deprecate } from "../deprecate/deprecate"

export const entries = Object.entries

export const fromEntries = deprecate(fromPairs, "fromEntries", "fromPairs")
