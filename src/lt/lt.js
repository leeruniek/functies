import { lt as _lt } from "ramda"

import { deprecate } from "../deprecate/deprecate"

export const lt = deprecate(_lt, "lt")
