import { clone as _clone } from "ramda"

import { deprecate } from "../deprecate/deprecate"

export const clone = deprecate(_clone, "clone")
