import { last as _last } from "ramda"

import { deprecate } from "../deprecate/deprecate"

export const last = deprecate(_last, "last")
