import { equals } from "ramda"

import { deprecate } from "../deprecate/deprecate"

export const deepEqual = deprecate(equals, "deepEqual", "equals")
