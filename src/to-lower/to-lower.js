import { toLower as _toLower } from "ramda"

import { deprecate } from "../deprecate/deprecate"

export const toLower = deprecate(_toLower, "toLower")
