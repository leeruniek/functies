import { identity } from "ramda"

import { deprecate } from "../deprecate/deprecate"

export const i = deprecate(identity, "i", "identity")
