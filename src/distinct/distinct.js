import { uniq } from "ramda"

import { deprecate } from "../deprecate/deprecate"

export const distinct = deprecate(uniq, "distinct", "uniq")
