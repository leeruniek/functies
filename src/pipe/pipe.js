import { pipe as _pipe } from "ramda"

import { deprecate } from "../deprecate/deprecate"

export const pipe = deprecate(_pipe, "pipe")
