import { gt as _gt } from "ramda"

import { deprecate } from "../deprecate/deprecate"

export const gt = deprecate(_gt, "gt")
