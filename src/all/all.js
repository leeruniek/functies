/* eslint-disable no-unused-vars */
// @flow

import { all as _all } from "ramda"

import { deprecate } from "../deprecate/deprecate"
import { byMatch } from "../is-match/is-match"

export const all = deprecate(_all, "all", "all")

/**
 * Test if object properties match all objects in input array
 *
 * @name       allBy
 * @tag        Core
 * @signature  (subset: Object) => (source: Object[]): boolean
 *
 * @param  {Object}  subset  Set of properties that should match
 * @param  {Array}   source  Input array
 *
 * @returns {boolean} True if all objects match, false otherwise
 *
 * @example
 * all({
 *   id: isString
 * })([
 *   { id: "uuid1", name: "foo"}
 *   { id: "uuid2", name: "bar"}
 * ])
 * // => true
 */
export const allBy = byMatch(all)
