export const deprecate = (fn, fnName, ramdaName = fnName, usageNote) => (
  ...args
) => {
  console.debug(
    `functies: The function "${fnName}" is deprecated.
Please use "${ramdaName}" from Ramda instead:
http://ramdajs.com/docs/#${ramdaName}${usageNote ? `\nNote: ${usageNote}` : ""}`
  )
  console.trace()

  return fn(...args)
}
