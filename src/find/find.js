import { find as _find } from "ramda"

import { deprecate } from "../deprecate/deprecate"
import { byMatch } from "../is-match/is-match"

export const find = deprecate(_find, "find")

/**
 * Find the first element that satisfies test function.
 *
 * @name       findBy
 * @tag        Array
 * @signature  (subset: Object) => (source: Object[]): mixed
 * @see        {@link find}
 * @see        {@link findIndexBy}
 *
 * @param  {Object}    subset  Set of properties that should match
 * @param  {Object[]}  source  Input array
 *
 * @returns {mixed|undefined}  First item that satisfied `fn`
 *
 * @example
 * findBy({
 *   id: 2
 * })([
 *   { id: 1, foo: "bar" },
 *   { id: 2, foo: "ipsum" }
 * ])
 * // => { id: 2, foo: "ipsum" }
 */
export const findBy = byMatch(_find)
