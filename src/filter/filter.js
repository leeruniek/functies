/* eslint-disable no-unused-vars */
// @flow

import { filter as _filter } from "ramda"

import { deprecate } from "../deprecate/deprecate"
import { byMatch } from "../is-match/is-match"

export const filter = deprecate(_filter, "filter")

/**
 * Filter items in array that match subset
 *
 * @name       filterBy
 * @tag        Array
 * @signature  (subset: Object) => (source: Object[]): Object[]
 *
 * @param  {Object}    subset  Set of properties that should match
 * @param  {Object[]}  source  Input array
 *
 * @returns  {Object[]}
 *
 * @example
 * filterBy({
 *   id: is,
 * })([
 *   { id: 1 },
 *   { id: null, name: "test" },
 *   { id: 3, foo: "bar" },
 *   { name: "test" },
 * ])
 * // => [{ id: 1 }, { id: 3, foo: "bar" }]
 */
export const filterBy = byMatch(_filter)
