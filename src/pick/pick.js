import { flip, pick as _pick, pickBy as _pickBy } from "ramda"

import { deprecate } from "../deprecate/deprecate"
import { uncurry } from "../curry/curry"

export const pick = deprecate(
  fn => source => _pickBy(flip(uncurry(fn)), source),
  "pick",
  "pickBy"
)

export const pickKeys = deprecate(_pick, "pickKeys", "pick")
