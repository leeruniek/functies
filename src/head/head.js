import { head as _head } from "ramda"

import { deprecate } from "../deprecate/deprecate"

export const head = deprecate(_head, "head")
