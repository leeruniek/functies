import { tail as _tail } from "ramda"

import { deprecate } from "../deprecate/deprecate"

export const tail = deprecate(_tail, "tail")
