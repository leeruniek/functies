import { pluck as _pluck } from "ramda"

import { deprecate } from "../deprecate/deprecate"

export const pluck = deprecate(_pluck, "pluck")
