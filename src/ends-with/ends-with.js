import { endsWith as _endsWith } from "ramda"

import { deprecate } from "../deprecate/deprecate"

export const endsWith = deprecate(_endsWith, "endsWith")
