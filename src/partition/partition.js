/* eslint-disable no-unused-vars*/

// @flow

import { partition as _partition } from "ramda"

import { deprecate } from "../deprecate/deprecate"
import { byMatch } from "../is-match/is-match"

export const partition = deprecate(_partition, "partition")

export const partitionBy = byMatch(_partition)
