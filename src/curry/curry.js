/* eslint-disable no-unused-vars*/
// @flow

import { curry as _curry } from "ramda"

import { is } from "../is/is"
import { deprecate } from "../deprecate/deprecate"
import type { UncurryType } from "./curry.js.flow"

export const curry = deprecate(_curry, "curry")

/**
 * Convert a curried function of arity n to a simple function with n parameters.
 *
 * @param fn
 * The function to apply
 *
 * @param args
 * The arguments to apply, in order
 *
 * @return
 * The result of calling the function on each argument in turn,
 * until a non-function is the return value. If an insufficient number
 * of arguments are provided, return the partially applied function.
 *
 * @example
 * uncurry(a => b => c => a + b * c)(1, 2, 3) = 7
 */
export const uncurry: UncurryType = fn => (arg, ...args) => {
  // Return the partially applied function if an insufficient number
  // of arguments are provided.
  if (!is(arg) && args.length === 0) {
    return fn
  }

  // Store the result of `fn(arg)`, because its type could change
  // between each invocation.
  const result = fn(arg)

  return typeof result === "function" ? uncurry(result)(...args) : result
}
