import { prop as _prop } from "ramda"

import { deprecate } from "../deprecate/deprecate"

export const prop = deprecate(_prop, "prop")
