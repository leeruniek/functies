import { sort as _sort } from "ramda"

import { deprecate } from "../deprecate/deprecate"
import { is } from "../is/is"

export const sort = deprecate(_sort, "sort")

/**
 * Sort an array of objects by a custom field
 *
 * @name   sortBy
 *
 * @tag Array
 * @signature ( field: string, direction: string ) => ( source: Array ): Array
 *
 * @param  {string}  field      Sort field name
 * @param  {string}  direction  Sort direction
 * @param  {Array}   source     Input array
 *
 * @return {Array}
 *
 * @example
 * sortBy( "position" )( [
 *   { id: 1, position: 3 },
 *   { id: 2, position: 2 },
 *   { id: 3 },
 *   { id: 4, position: 5 },
 *   { id: 5, position: null },
 * ] )
 * // [
 * //  { id: 2, position: 2 },
 * //  { id: 1, position: 3 },
 * //  { id: 4, position: 5 },
 * //  { id: 5, position: null },
 * //  { id: 3 },
 * //]
 */
export const sortBy = (field, direction = "asc") => source => {
  const result = Array.isArray(source) ? [...source] : [source]

  return result.sort((alice, bob) => {
    const aliceValue = is(alice[field])
      ? alice[field]
      : direction === "asc"
      ? Number.POSITIVE_INFINITY
      : Number.NEGATIVE_INFINITY

    const bobValue = is(bob[field])
      ? bob[field]
      : direction === "asc"
      ? Number.POSITIVE_INFINITY
      : Number.NEGATIVE_INFINITY

    return alice[field] === null && typeof bob[field] === "undefined"
      ? -1
      : aliceValue < bobValue
      ? direction === "asc"
        ? -1
        : 1
      : direction === "asc"
      ? 1
      : -1
  })
}
