import { forEach as _forEach } from "ramda"

import { deprecate } from "../deprecate/deprecate"

export const forEach = deprecate(_forEach, "forEach")
