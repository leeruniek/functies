import { split as _split } from "ramda"

import { deprecate } from "../deprecate/deprecate"

export const split = deprecate(_split, "split")
