/* eslint-disable no-unused-vars */
// @flow

import { flip as _flip } from "ramda"

import { deprecate } from "../deprecate/deprecate"
import type { FlipType, FlipUncurriedType } from "./flip.js.flow"

/**
 * Reverse the first two parameters of a function.
 */
export const flip: FlipType = <A, B, C>(f) => a => b => f(b)(a)

export const flipUncurried = deprecate(_flip, "flipUncurried", "flip")
