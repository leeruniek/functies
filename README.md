# functies [![npm package version](https://badge.fury.io/js/%40leeruniek%2Ffuncties.svg)](https://badge.fury.io/js/%40leeruniek%2Ffuncties) [![Coverage Status](https://coveralls.io/repos/gitlab/leeruniek/functies/badge.svg?branch=master)](https://coveralls.io/gitlab/leeruniek/functies?branch=master)

> Functional library for Javascript 

---

<!-- vim-markdown-toc GFM -->

* [Install](#install)
* [Use](#use)
* [Develop](#develop)
* [Docs](#docs)
* [Changelog](#changelog)
  * [0.34.2 - 13 Nov 2019](#0341---13-nov-2019)
    * [Deprecate](#deprecate)

<!-- vim-markdown-toc -->

## Install

```bash
npm i --save-exact @leeruniek/functies
```

## Use

```js
import { pipe, trim, split, dropLast, push, join } from "@leeruniek/functies"

const removeTrailingSlash = source =>
  source[source.length - 1] === sep ? source.slice(0, -1) : source

const renameFile = newName => pipe(
  removeTrailingSlash,
  split(sep),
  dropLast,
  push(trim(sep)(newName)),
  join(sep)
)
```

## Develop

```bash
git clone git@github.com:leeruniek/functies.git && \
  cd m && \
  npm run setup

# run tests (any `*.test.js`) once
npm test

# watch `src` folder for changes and run test automatically
npm run tdd
```

## Docs

See [documentation](https://leeruniek.github.io/functies/) for the awesomeness that we are.

## Changelog

History of all changes in [CHANGELOG.md](CHANGELOG.md)

### 0.34.2 - 13 Nov 2019

Deprecate several functions which have an equivalent in
[Ramda](https://ramdajs.com/).

The affected functions have the same API and will continue to work for now,
but will emit a debug message suggesting to use the Ramda version. The docs
have also been removed for these functions, to discourage using them in new
code.

#### Deprecate

- `all`, `clone`, `curry`, `deep-equal`, `distinct`, `endsWith`, `fromEntries`,
  `filter`, `find`, `flattenOne`, `flatten`, `flipUncurried`, `forEach`, `get`,
  `gt`, `hasKey`, `head`, `i`, `ifThen`, `last`, `partition`, `pick`,
  `pickKeys`, `pipe`, `pluck`, `prop`, `sort`, `split`, `tail`, `toLower`,
  `type`, `zip`, `zipWith`
